\newcommand{\Ppart}{\pazocal{P}}
\newcommand{\Qpart}{\pazocal{Q}}
\newcommand{\htop}{h_\text{top}}

\subsection{Entropia misurabile}

\textbf{(Lemma di Fakete).} Sia $a_{n}$ una successione subadditiva. Allora $\lim_{n}\frac{a_{n}}{n} = \inf_{n} \frac{a_{n}}{n}$ esiste.

\textbf{(Entropia del raffinamento).} $H(\Ppart \vee \Qpart) \leq H(\Ppart) + H(\Qpart)$.

\textbf{(Entropia di una partizione dato il sistema dinamico).} $h(f, \Ppart) \eqdef \frac{1}{T}H(\Ppart_{T})$.

\textbf{(Entropia di Kolmorov-Sinai)} $h_{\text{KS}}(f) \eqdef \sup_{\Ppart}h(f, \Ppart)$.

\textbf{(Teorema).} Se $\Ppart$ è una partizione generatrice, allora $h_{\text{KS}}(f) = h(f, \Ppart)$.

\subsection{Entropia topologica}

\textbf{(Entropia topologia).} $\htop = \lim_{\epsilon} \limsup_{n}\frac{1}{n}\log\text{span}(n, \epsilon)$.

\textbf{(Insiemi separati).} $\text{sep}(n, 2\epsilon) \leq \text{span}(n, \epsilon) \leq \text{sep}(n, \epsilon)$.

\textbf{(Ricoprimenti aperti).} Dato un ricoprimento aperto $\alpha$, sia $N(\alpha)$ la cardinalità massima di un sottoricoprimento di $\alpha$. Allora vale che $N(\alpha \vee \beta) \leq N(\alpha)N(\beta)$ e $N(f^{-1}(\alpha)) \leq N(\alpha)$. Inoltre $\htop = \sup_{\alpha}\lim_{n}\frac{1}{n}\log N(\bigvee_{0}^{n-1}f^{-i}(\alpha))$

\textbf{(Metriche equivalenti).} L'entropia topologica non dipende dalla metrica che induce la topologia. Quindi è invariante per coniugio topologico.

\textbf{(Entropia topologica e misurabile).} Siano $(X, d)$ spazio metrico compatto e $f: X \rightarrow X$ continua. Sia $\Ppart_{f}(X)$ l'insieme delle misure di probabilità $f$-invarianti. Allora $\htop(f) = \sup_{\mu \in \Ppart_{f}(X)}h_{\text{KS}}(f, \mu)$.

\textbf{(Perron-Frobenius per matrici non negative).} Sia $A\geq 0$, allora esiste $\lambda_{A} \geq 0$ autovalore tale che $|\lambda| \leq \lambda_{A}$ per ogni altro autovalore $\lambda$. Inoltre i corrispondenti autovettori destro e sinistro sono non negativi.

\textbf{(Mappe espansive).} $f: X\rightarrow X$ si dice espansiva se esiste un $\delta$ tale che per ogni $x, y$ con $x \neq y$ esiste un $N$ tale che $d(f^{N}(x), f^{N}(y)) \geq \delta$. Se $f$ è una mappa espansiva allora $\htop(f) = h_{\epsilon}(f)$ per ogni $\epsilon < \delta$, dove $h_{\epsilon}(f) = \limsup_{n}\frac{1}{n}\log\text{span}(n, \epsilon)$.

\textbf{(Catena di Markov topologica).} Se $A$ è primitiva ed è la matrice di adiacenza  di un grafo, la catena di Markov topologica $(\Sigma_{A}, d, \sigma_{A})$ ha entropia topologica $\htop(\sigma_{A}) = \ln(\lambda_{A})$.

\textbf{(Norma di una matrice).} La norma di una matrice $\norm{\cdot}: K^{m\times n} \rightarrow \mathbb{R}$ che soddisfa le seguenti proprietà. Per tutti gli scalari $\alpha \in K$ e per tutte le matrici $A, B \in K^{m\times n}$:
\begin{itemize}
    \item $\norm{\alpha A} = |\alpha|\norm{A}$ 
    \item $\norm{A+B} \leq \norm{A}+\norm{B}$
    \item $\norm{A} \geq 0$ 
    \item $\norm{A} = 0 \iff A = 0$
\end{itemize}

\textbf{(Norme indotte dalla norma dei vettori).} $\norm{A}_{p} = \sup_{x\neq 0}\frac{\norm{Ax}_{p}}{\norm{x}_{p}}$ 

\textbf{(Osservazione).} Le norme indotte dalla norma dei vettori sono submultiplicative, cioè $\norm{AB} \leq \norm{A}\norm{B}$. \textit{Idea:} banale. Inoltre data una norma indotta dalla norma dei vettori vale che $\norm{A^{r}}^{\frac{1}{r}} \geq \rho(A)$.

\textbf{(Norma di Frobenius).} $\norm{A}_{F}^{2}=\sum_{ij}|A_{ij}|^{2}$. Vale che $\norm{Ax}_{2} \leq \norm{A}_{F}\norm{x}_{2}$. La norma di Frobenius è submultiplicativa.

\textbf{(Equivalenza tra norme)} Date due norme $\norm{\cdot}_{\alpha}$ e $\norm{\cdot}_{\beta}$ si ha che esistono due costanti che legano le due norme. In particolare tutte le norme inducono la stessa topologia. 

\textbf{(Markov shifts misurabili).} Il sistema dinamico misurabile $(\Sigma, \sigma, \mu)$ è fortemente mescolante. Inoltre $h_{\text{KS}}(\sigma) = -\sum_{i,j}p_{i}P_{ij}\ln P_{ij}$.

\textbf{(Osservazione).} Il sistema dinamico associato a un Markov shift misurabile è ergodico se e solo se la matrice $P$ è irriducibile (il grafo è connesso). $Idea:$ se $P$ è irriducibile il sistema è fortemente mescolante, quindi mescolante in media, quindi ergodico. Se per assurdo $P$ non fosse irriducibile, il grafo sarebbe sconnesso e si contraddice la definizione di sistema ergodico.

\subsection{Information theoretic entropy}
\textbf{(Entropia di un processo stocastico).} $H(X) = \lim\frac{1}{N}H(X_{1}, \dots, X_{n})$.

\textbf{(Entropia di un processo stazionario).} Se il processo stocastico è stazionario il limite sopra esiste e vale $\lim H(X_{n}|X_{n-1}, \cdots, X_{1})$.

\textbf{(Entropia di un processo di Markov stazionario).} $H(X_{2}|X_{1}) = \sum_{i,j}p_{i}P_{ij}\ln P_{ij}$.



