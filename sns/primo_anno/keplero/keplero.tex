\documentclass{article}
\usepackage{/home/checco/packs/general}
\usepackage{mathtools}

\usepackage[symbol]{footmisc}
\renewcommand{\thefootnote}{\fnsymbol{footnote}}

\title{Il problema dei due corpi visto come moto libero su una sfera 4D}

\begin{document}
\maketitle
Il seguente problema è stato proposto da Federico Belliardo a tutorato di fisica del primo anno. Queste note sono redatte da Francesco Debortoli\footnote[2]{francesco.debortoli@sns.it}. \\

\textbf{Problema.} Dati due corpi che interagiscono gravitazionalmente è noto che esistono delle quantità vettoriali che si conservano. Nel sequente problema ci si propone di riparametrizzare il moto dei due corpi e mostrare che il moto è equivalente a quello di un punto libero su una sfera 4D. Di conseguenza il momento angolare generalizzato si conserva e noteremo che questa conservazione corrisponde alla conservazione del momento angolare classico e del vettore di Lenz. \\ 
\\Innanzitutto sappiamo che il problema dei due corpi è equivalente al moto di un punto materiale di massa ridotta $\mu$ nel potenziale gravitazionale dato dalla massa totale $M$. Sia $(t, x_1, x_2, x_3)$ la traiettoria seguita dal punto. Prendiamo $GM \mu =1$ e l'energia totale $E = -1/2$ scegliendo le opportune unità di misura. Consideriamo una opportuna riparametrizzazione $(t(\lambda), x_1(\lambda), x_2(\lambda), x_3(\lambda))$ in funzione di $\lambda$, tale per cui $t' \eqdef \frac{dt}{d\lambda} = r$, dove chiaramente $r$ è la distanza dall'origine. Definiamo inoltre $\vec{r}\;'(\lambda) \eqdef \frac{d \vec{r}(\lambda)}{d \lambda}$.

\begin{enumerate}

\item Mostrare che il vettore $(t', \vec{r}\;')$ sta su una sfera.

\item Ricordando che la legge del moto è $\ddot{\vec{r}} = - \frac{\vec{r}}{r^3}$ mostrare che
\begin{align*}
t''' &= 1-t' \\
\vec{r}\;''' &= -\vec{r}\;' 
\end{align*}

\item Si consideri il vettore $\vec{s} = (t'-1)\hat{e}_t + \vec{r}\;'$ nello spazio $(t, x_1, x_2, x_3)$. Mostrare che questo al variare del parametro $\lambda$ descrive il moto di un punto libero su una sfera e quindi percorre dei cerchi massimi su un piano.

\item Si consideri il tensore $\Lambda_{ij} = s_i s_j' - s_j s_i'$. Mostrare che è costante nel tempo.

\item Siano $\vec{L}$ e $\vec{Y}$ rispettivamente il momento angolare e il vettore di Lenz associati all'orbita originiaria della massa $\mu$ nel campo di $M$. Mostrare che il tensore $\Lambda$ ha la forma:
\begin{align*}
\Lambda = 
\begin{pmatrix*}[r]
0		& Y_1 	& Y_2 	& Y_3 \\
-Y_1	&   0 	& L_3 	&-L_2 \\
-Y_2 	& -L_3	&   0 	& L_1 \\
-Y_3  	& L_2   & -L_1	& 0 \\
\end{pmatrix*}
\end{align*}

\end{enumerate}

\textbf{Soluzione.}

\begin{enumerate}

\item Sia $\vec{r} = (x_1, x_2, x_3)$. Dalla regola della catena si ottiene che $\dot{x}_i = \frac{{x}_i'}{t'}$ dove ovviamente $x_i' \eqdef \frac{dx_i}{d\lambda}$. La conservazione dell'energia si scrive come $\frac{1}{2} \dot{x}_i \dot{x}_i - \frac{1}{r} = -\frac{1}{2}$ da cui si ricava $x_i'x_i' + (t'-1)^2 = 1$.

\item Applicando più volte la regola della catena si ottiene:
\begin{align*}
\vec{r}\;' &= r\dot{\vec{r}} \\
\vec{r}\;'' &= r(\dot{r}\dot{\vec{r}}+r\ddot{\vec{r}}) = r\dot{r}\dot{\vec{r}} - \frac{\vec{r}}{r} \\
\vec{r}\;''' &= r \dot{\vec{r}} \left(\dot{r}^2+r\ddot{r}-\frac{1}{r}\right)
\end{align*}

Sapendo che $r^2 = \vec{r}\vec{r}$, derivando rispetto al tempo si ottiene che $r\dot{r} = \vec{r}\dot{\vec{r}}$ e $r\ddot{r}  + \dot{r}^2= \vec{r}\ddot{\vec{r}} + \dot{\vec{r}}\;^2 = \dot{\vec{r}}\;^2 - \frac{1}{r}$, da cui usando la conservazione dell'energia si conclude che $r\ddot{r}  + \dot{r}^2 = -1 + \frac{1}{r}$ e quindi $\vec{r}\;''' = -\vec{r}\;'$. \\
Si ha che
\begin{align*}
r' &= r \dot{r} \\
r'' &= r(\dot{r}^2+r\ddot{r}) = 1-r
\end{align*}
quindi $t''' = 1-t$.

\item Abbiamo che $||\vec{s}\;|| = 1$ per il punto 1) e che $\vec{s}\;'' = -\vec{s}$ per il punto 2). La soluzione generale è quindi $\vec{s} = \vec{A} \cos(\lambda) + \vec{B} \sin(\lambda)$, che corrisponde proprio al moto libero vincolato ad una sfera di raggio 1.

\item Si ha $\frac{d\Lambda_{ij}}{dt} = r \frac{d\Lambda_{ij}}{d\lambda} = s_i's_j'+s_is_j'' - (s_j's_i' + s_js_i'') = -s_is_j + s_js_i = 0$


\item Ricordiamo che $\vec{s} = (t'-1,\; x_1',\; x_2',\; x_3')$. Dalla definizione di $\Lambda_{ij}$ si ha per $1\leq j \leq 3$ che $\Lambda_{0j} = s_0 s_j' - s_j s_0' = (t'-1) x_j''-x_j' t''$
e ricordando che $t' = r$ e $t'' = r'$ si ha che $\Lambda_{0j} = (r-1) x_j'' - x_j'r'$. Applicando la regola della catena si trovano le relazioni:
\begin{align*}
x_j'  &= t' \dot{x}_j  = r\dot{x}_j \\
x_j'' &= r \frac{d x_j'}{dt} = r(\dot{r}\dot{x}_j + r \ddot{x}_j) \\
r' &= r\dot{r}
\end{align*}

Quindi $\Lambda_{0j} = (r-1)r(\dot{r}\dot{x}_j + r \ddot{x}_j) - \dot{x}_jr^2\dot{r}$. \\ 
Il vettore di Lenz vale $\vec{Y} = \dot{\vec{r}} \times \vec{L} - \hat{r}$. Inoltre valgono:
\begin{align*}
\hat{r} &= \frac{x_i}{r}\hat{x}_i \\
\dot{\vec{r}} &= \dot{x}_i \hat{x}_i \\
\vec{L} &= (x_l \hat{x}_l) \times {\dot{x}_m \hat{x}_m} = x_l \dot{x}_m \epsilon_{lmn} \hat{x}_n \\
\dot{\vec{r}} \times \vec{L} &= \dot{x}_i x_l \dot{x}_m \epsilon_{lmn} \epsilon_{ino} \hat{x}_o \\
\epsilon_{lmn} \epsilon_{ino} &= \epsilon_{nlm} \epsilon_{noi} = \delta_{lo} \delta_{mi} - \delta_{li}\delta_{mo}
\end{align*}
da cui si ottiene $Y_j = \dot{x}_i\dot{x}_i x_j - \dot{x}_i x_i \dot{x}_j - \frac{x_j}{r}$. \\
Dalle equazioni del moto sappiamo che $\ddot{x}_j = - \frac{x_j}{r^3} $, sostituendo si ottiene:
\begin{align*}
\Lambda_{0j} = -x_j - r\dot{r}\dot{x}_j + \frac{x_j}{r}
\end{align*}
Dalla conservazione dell'energia si ha che $\dot{x}_i\dot{x}_i = -1 +\frac{2}{r}$ e derivando rispetto al tempo si ottiene $\dot{x}_i\ddot{x_i} = -\frac{r}{\dot{r}}$ e sostituendo le equazioni del moto si ottiene $\dot{x}_i x_i = r \dot{r}$.
Infine $\Lambda_{0j} = -x_j - r\dot{r}\dot{x}_j - \frac{x_j}{r} + \frac{2}{r}x_j = -x_j - r\dot{r}\dot{x}_j - \frac{x_j}{r} + (\dot{x}_i\dot{x}_i + 1)x_j = Y_j  $. \\
Per $1\leq i \leq 2$ e $2\leq j \leq 3$ vale $\Lambda_{ij} = x_i' x_j'' - x_j' x_i''$. Con le sostituzioni viste in precedenza si trova $\Lambda_{ij} = x_i \dot{x}_j - x_j \dot{x}_i$. Ma quindi $\Lambda_{ij} = x_l \dot{x}_m \epsilon_{lmn} \epsilon_{ijn} = L_n \epsilon_{ijn}$. \\
Notando che il tensore $\Lambda$ ha la diagonale nulla ed è antisimmetrico possiamo concludere che
\begin{align*}
\Lambda = 
\begin{pmatrix*}[r]
0		& Y_1 	& Y_2 	& Y_3 \\
-Y_1	&   0 	& L_3 	&-L_2 \\
-Y_2 	& -L_3	&   0 	& L_1 \\
-Y_3  	& L_2   & -L_1	& 0 \\
\end{pmatrix*}
\end{align*}

\end{enumerate}

\end{document}
