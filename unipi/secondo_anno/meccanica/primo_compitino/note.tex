\documentclass{article}
\usepackage{/home/checco/packs/general}

\begin{document}
    \section{Meccanica Lagrangiana}
    \textbf{(Particella in campo magnetico).}
    \begin{align*}
        L &= \frac{1}{2}m |\mathbf{v}|^{2} -q\phi(\mathbf{x}, t) +q \mathbf{v} \cdot \mathbf{A}(\mathbf{x}, t) \\
        H &= \frac{1}{2m}|\mathbf{p}-q\mathbf{A}|^{2}+q\phi
    \end{align*}

    \textbf{(Achtung!)} La Lagrangiana scritta così è sempre vera, usando i momenti cinetici come $\mathbf{p}$ (anche in cilindriche, per capirci). L'Hamiltoniana scritta così vale solo nel caso in cui $\mathbf{p}$ corrispondono ai momenti cinetici (si vede nella derivazione). Se devo calcolare l'Hamiltoniana in coordinate cilindriche, parti sempre dalla Lagrangiana e poi fai la trasformata di Legendre.

    \textbf{(Teorema di Noether).}
    Se la Lagrangiana è invariante al primo ordine per la trasformazione $q_{\alpha} \rightarrow q_{\alpha}' = q_{\alpha}+\epsilon A_{\alpha}(q)$ allora si conserva la quantità $p_{\alpha}A_{\alpha}(q)$.

    \textbf{(Invariante adiabatico dell'oscillatore armonico).} Data l'Hamiltoniana $H = \frac{p^{2}}{2m}+\frac{m\omega^{2}}{2}q^{2}$, l'invariante adiabatico $\pazocal{I}(E) = \frac{2\pi E}{\omega}$. Inoltre $E =  \frac{m\omega^{2}}{2}A^{2}$ quindi $\pazocal{I}(E) = \pi m \omega A^{2}$.

    \subsection{Piccole oscillazioni}
    \textbf{($T$ e $U$-ortogonalità).} Autovettori relativi a autovalori distinti sono sempre $T$ e $U$-ortogonali. (Si usa la simmetria di $T$ e $U$). Utile per torvare l'ultimo autovettore.

    \textbf{(Achtung!)} Se per esempio ho due particelle che si attraggono elettricamente e mi viene chiesta una condizione su $v$ affinché il sistema si mantenga in regime di piccole oscillazioni è necessario che la coordinata $x_{\text{relativo}}$ si mantenga piccola. Quindi conviene separare il moto del centro di massa.

    \textbf{(Simmetrie).} Se la Lagrangiana rispetta una matrice di simmetria $S$, preso un autovettore non degere questo è anche un autovettore di $S$ (perché per simmetria $Sv$ è un autovettore con la stessa frequenza, ma quindi essendo l'autospazio non degenere $Sv = \lambda v$). Quindi conviene sempre partire con la ricerca degli autovettori di $S$. Per esempio nel caso in cui ci siano tre variabili $x_{1}, x_{2}, x_{3}$ e $x_{2} \leftrightarrow x_{3}$ allora cerco gli autovettori della matrice 
    \begin{align*}
        \begin{bmatrix}
            1 & 0 & 0 \\
            0 & 0 & 1 \\
            0 & 1 & 0 \\
        \end{bmatrix}
    \end{align*}

    \textbf{(Matrice degli autovettori)}. Supponiamo di avere un problema a $N$ gradi di libertà. Supponiamo di aver trovato $N$ autovettori $A^{1}, \dots, A^{N}$. Consideriamo la matrice costruita usandoli come colonne:
    \begin{align*}
        \begin{bmatrix}
            A^{1} \cdots A^{N}
        \end{bmatrix}
    \end{align*}
    Facciamo un'osservazione preliminare: $(A^{T}MA)_{ij} = {A^{i}}^{T}MA_{j}$, ovvero il prodotto scalare tra gli autovettori $i, j$-esimi. Possiamo quindi definire l'$i$-esimo modo normale $Q_{i}$ come 
    \begin{align*}
        \begin{bmatrix}
            x_{1} \\
            \cdots \\
            x_{N}
        \end{bmatrix}
        = A 
        \begin{bmatrix}
            Q_{1} \\
            \cdots \\
            Q_{N}
        \end{bmatrix}
    \end{align*}
    A questo punto la Lagrangiana è diagonalizzata perché gli autovettori $A^{i}$ possono essere trovati $T$ e $U$-ortonormali. A questo punto può essere utile chiedersi come è fatta la matrice $A^{-1}$. Se una tra $T$ e $U$ è proporzionali all'identità allora $A$ sarà una matrice ortogonale e quindi $A^{-1} = A^{T}$. Questo in generale non è vero (vedi \texttt{MC09/01/2019}).

    \textbf{(Matrice cinetica generica).} Nel calcolo dei modi normali la matrice cinetica va calcolata nel punto di equilibrio, perché ci stiamo limitando a sviluppare in $O(\epsilon^{2})$ e le velocità sono già $O(\epsilon)$. Facciamo un esempio: immaginiamo il moto di una particella in campo centrale con potenziale $U(r)$. La Lagrangiana si scrive quindi come
    \begin{align*}
        L = \frac{1}{2}m(\dot{r}^{2} + r^{2}\dot{\theta}^{2}) - U(r)
    \end{align*}
    e supponiamo che per $\bar{r}$ si ha $\pdv{U}{r}$$(\bar{r}) = 0$. In questo caso si ha che
    \begin{align*}
        T &=
        \begin{bmatrix}
            1 & 0 \\
            0 & \bar{r} \\
        \end{bmatrix}
        \\
        U &=
        \begin{bmatrix}
            \frac{\partial^{2}U}{\partial r^{2}}(\bar{r}) & 0 \\
            0 & 0
        \end{bmatrix}
    \end{align*}
    quindi per $\theta$ c'è un'autofrequenza degenere pari a $0$.

    \textbf{(Coordinate del centro di massa)} Presi
    \begin{align*}
        \mathbf{R} &\eqdef \frac{m_{1}\mathbf{r}_{1}+m_{2}\mathbf{r}_{2}}{m_{1}+m_{2}} \\
        \mathbf{r} &\eqdef \mathbf{r}_{2} - \mathbf{r}_{1}
    \end{align*}

    vale che 
    \begin{align*}
        \frac{1}{2}m_{1}|\vel{r}_{1}|^{2} + \frac{1}{2}m_{2}|\vel{r}_{2}|^{2} = \frac{1}{2}M|\vel{R}|^{2}+\frac{1}{2}\mu|\vel{r}|^{2}
    \end{align*}

    \textbf{(Invarianti adiabatici che sembrano un  mix).} Se ci sono uno o più modi normali mescolati fa lo stesso, l'importante è che variando il parametro venga influenzata solamente la frequenza di un solo modo normale.

    \section{Meccanica Hamiltoniana}
    \textbf{Equazioni di Hamilton}
    \begin{align*}
        \pdv{H}{p_{\alpha}} &= \dot{q}_{\alpha} \\
        \pdv{H}{q_{\alpha}} &= -\dot{p}_{\alpha}
    \end{align*}
    \textbf{Osservazioni.}
    \begin{itemize}
        \item Quando faccio una trasformazione $p \rightarrow P, q \rightarrow Q$ che non dipende dal tempo, l'hamiltoniana rimane invariata in valore: per trovare $K(Q, P)$ mi basta riscrivere $H(q, p)$ in funzione di $(Q, P)$. In particolare vale:
            \begin{equation*}
                \pdv{\Delta H}{Z_{i}} = -\Gamma_{ik}\pdv{Z_{k}}{t}
            \end{equation*}
        Nota come non dipende dalla hamiltoniana ma solo dalla trasformazione.

    \item L'inverso di una trasformazione canonica è canonica. \textit{Idea:} si vede subito dalla simpletticità della matrice jacobiana.

    \item Se $H$ è separabile e indipendente dal tempo, cioè posso scrivere $H = \sum_{i}H_{i}(q_{i}, p_{i})$, allora le $H_{i}$ sono costanti del moto, infatti $[H_{i}, H] = [H_{i}, H_{i}] = 0$.

    \item Nel caso dell'oscillatore armonico $n$-dimensionale $H = \frac{\mathbf{p}^{2}}{2m}+\frac{m\omega^{2}}{2}\mathbf{q}^{2}$ si conserva il tensore simmetrico $A_{ij} = \frac{p_{i}p_{j}}{2m}+\frac{m\omega^{2}}{2}q_{i}q_{j}$. Le nove componenti sono i generatori del gruppo di simmetria $U(3)$.
    Nel caso bidimensionale valgono le seguenti relazioni:
    \begin{align*}
        H_{1}H_{2} &= A_{12}^{2}+\frac{\omega^{2}}{4}L^{2} \\
        S_{1} &\eqdef \frac{A_{12}+A_{21}}{2\omega} \\
        S_{2} &\eqdef \frac{A_{22}-A_{11}}{2\omega} \\
        S_{3} &\eqdef \frac{L}{2} \\ 
        S_{1}^{2}+S_{2}^{2}+S_{3}^{2} &= \frac{H^{2}}{4\omega^{2}}
    \end{align*}

    \textbf{(Scomporre $H$ lungo gli assi).}
    \begin{align*}
        H = \frac{|\pos{p}_{1}|^{2} }{2m_{2}}+\frac{|\pos{p}_{2}|^{2} }{2m_{2}}+\frac{|\pos{p}_{3}|^{2} }{2m_{3}} + V(\pos{x}_{1}, \pos{x}_{2}, \pos{x}_{3})
    \end{align*}
    dove nel potenziale compaiono solamente somme, differenze e prodotti scalari. Allora posso scrivere $H = H_{x} + H_{y} + H_{z}$ perché le coordinate non si mischiano.

    \end{itemize}

    \textbf{(Costanti del moto).} Un oscillatore armonico con tre frequenze diverse lungo gli assi ha solo $3$ integrali del moto (le energie dei 3 oscillatori separati). Un oscillatore armonico isotropo ha $5$ integrali del moto (la terza componente del momento angolare non è indipendente, infatti $[l_{1}, l_{2}] = l_{3}$ e si vede che $l_{3}$ si conserva usando l'identità di Jacobi).

    \subsection{Tricks per speedrunnare i conti}
    \begin{align*}
        \nabla |\mathbf{r}-\mathbf{r'}| = \frac{\mathbf{r}-\mathbf{r'}}{|\mathbf{r}-\mathbf{r'}|}
    \end{align*}

    Data una funzione $\mathbf{f} = (f_{1}, f_{2}, f_{3})$ intendiamo $[\mathbf{f}, G] = ([f_{1}, G], [f_{2}, G], [f_{3}, G])$. Allora vale che
    \begin{align*}
        [\mathbf{a}\times\mathbf{b}, G] &= \mathbf{a}\times[\mathbf{b}, G] + [\mathbf{a}, G]\times\mathbf{b} \\
        [\mathbf{a}\cdot\mathbf{b}, G] &= \mathbf{a}\cdot[\mathbf{b}, G] + [\mathbf{a}, G]\cdot\mathbf{b}
    \end{align*}

    \subsection{Parentesi di Poisson}
    \begin{itemize}
        \item $[f_{1}f_{2}, g] = f_{1}[f_{2}, g] + [f_{1}, g]f_{2}$

        \item Data una generica funzione $f(q, p, t)$ vale che $\frac{d}{dt}f = [f, H] + \pdv{f}{t}$.

    \end{itemize}

    \subsection{Funzioni generatrici}
    Basta ricordarsi
    \begin{align*}
        pdq-Hdt = PdQ -Kdt + dF_{1}
    \end{align*}
    $\mathbf{F_{2}}$
    \begin{align*}
        \pdv{F_{2}}{q_{\alpha}} &= p_{\alpha} \\
        \pdv{F_{2}}{P_{\alpha}} &= Q_{\alpha} \\
        \pdv{F_{2}}{t} &= K-H
    \end{align*}

    \begin{itemize}
        \item $G$ è invariante sotto evoluzione temporale se e solo se l'hamiltoniana risulta invariante sotto trasformazione infinitesima generata da $G$.
    \end{itemize}

    \textbf{(Trasformazioni infinitesime).} Consideriamo una trasformazione infinitesima che agisce su un vettore:
    \begin{align*}
        \delta v = \delta\theta \Omega v
    \end{align*}

    per esempio una rotazione attiva nel piano:
    \begin{align*}
        \Omega = 
        \begin{bmatrix}
            0 & -1 \\
            1 & 0 
        \end{bmatrix}
    \end{align*}
    Allora possiamo pensare di iterare la trasformazione infinitesima, applicandola $N = \frac{\theta}{\delta\theta}$ volte. Quindi
    \begin{align*}
        v' = \left(1+\frac{\theta}{N}\Omega\right)^{N} = e^{\theta\Omega}
    \end{align*}

\end{document}
